#!/bin/bash

# DB情報
db_name='db_name'
db_user='db_user'
db_password=''

# バックアップディレクトリ
backup_directory='/path/to/backup/'

# 日付フォーマット
date=`date +%Y%m%d`

# バックアップディレクトリがなかったら作成
if [ ! -d ${backup_directory} ]
then
mkdir -p ${backup_directory}
fi

# mysqldumpでDBをSQLに吐き出す
mysqldump -u ${db_user} -p${db_password} $db_name | gzip > ${backup_directory}/${db_name}_${date}.sql.gz

# バックアップディレクトリ配下で7日以前のものを削除する
find ${backup_directory} -ctime +7 -exec rm {} \;

