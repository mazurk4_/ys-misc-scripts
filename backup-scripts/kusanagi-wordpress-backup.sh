#!/bin/bash

PATH=/usr/local/sbin:/usr/bin:/bin

# profile
PROFILE_NAME=''

# 日付
date=`date +%Y%m%d`

# バックアップ先
BACKUP_DIR=/path/to/backup/${PROFILE_NAME}/

# バックアップ対象
TARGET_DIR=/home/kusanagi/${PROFILE_NAME}/DocumentRoot/wp-content

# バックアップ後のファイル名
BACKUP_FILE_NAME=${PROFILE_NAME}-bk_${date}.tar.gz

# バックアップディレクトリ確認
if [ ! -d ${BACKUP_DIR} ]
  then
  mkdir -p ${BACKUP_DIR}
fi

# バックアップ処理
cd $TARGET_DIR
tar czfp $BACKUP_FILE_NAME ./ || [[ $? == 1 ]]
mv $BACKUP_FILE_NAME $BACKUP_DIR

# 古いバックアップを削除
find ${BACKUP_DIR} -ctime +7 -exec rm {} \;
