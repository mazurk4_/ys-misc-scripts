#!/bin/bash

REMOTEHOSTS="hostname"
#PASSWD="----------------"
PORT="SSH-PORT"
USER="SSH-USER"
KEY="SSH-KEY"

## dir ##
BACKUPDIR="/path/to/backup-target"
REMOTEDIR="/path/to/remote"


rsync --delete -r -ave "ssh -p $PORT -o 'StrictHostKeyChecking no' -i $KEY" $BACKUPDIR $USER@$REMOTEHOSTS:$REMOTEDIR
