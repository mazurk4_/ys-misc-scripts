#!/bin/bash

# DB情報
db_user=''
db_password=''

# バックアップディレクトリ
backup_directory='/path/to/backup-full/'

# 日付フォーマット
date=`date +%Y%m%d`

# バックアップディレクトリがなかったら作成
if [ ! -d ${backup_directory} ]
then
mkdir -p ${backup_directory}
fi

# mysqldumpでDBをSQLに吐き出す
mysqldump -u ${db_user} -h localhost -p${db_password} -A | gzip > ${backup_directory}/fulldump_${date}.sql.gz

# バックアップディレクトリ配下で21日以前のものを削除する
find ${backup_directory} -ctime +22 -exec rm {} \;
